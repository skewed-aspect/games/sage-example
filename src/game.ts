// ---------------------------------------------------------------------------------------------------------------------
// Example Game
// ---------------------------------------------------------------------------------------------------------------------

import { Engine, Scene, FreeCamera, Vector3, HemisphericLight, Color3, MeshBuilder } from 'babylonjs';
import 'babylonjs-loaders';
import * as GUI from 'babylonjs-gui';

// Base Models
import { BaseGame } from '@skewedaspect/sage';

// ---------------------------------------------------------------------------------------------------------------------

class ExampleGame extends BaseGame
{
    constructor()
    {
        super();

        //TODO: Do stuff
    } // end constructor

    private _canvas ?: HTMLCanvasElement;
    private _engine ?: Engine;
    private _scene ?: Scene;
    private _camera ?: FreeCamera;

    start(canvas : HTMLCanvasElement) : void
    {
        this._canvas = canvas;
        this._engine = new Engine(this._canvas, true);
        this._engine.setHardwareScalingLevel(1 / window.devicePixelRatio);

        this._scene = this.createScene(this._engine, canvas);
        this.render(this._engine, this._scene);
    } // end start

    createScene(engine : Engine, canvas : HTMLCanvasElement) : Scene
    {
        // Create a basic BJS Scene object.
        const scene = new Scene(engine);

        // Create a FreeCamera, and set its position to (x:0, y:5, z:-10).
        this._camera = new FreeCamera('camera1', new Vector3(0, 5, -10), scene);

        // Target the camera to scene origin.
        this._camera.setTarget(Vector3.Zero());

        // Attach the camera to the canvas.
        this._camera.attachControl(canvas, false);

        // Create a basic light, aiming 0,1,0 - meaning, to the sky.
        const light = new HemisphericLight('light1', new Vector3(0, 1, 0), scene);
        light.intensity = 0.25;
        light.groundColor = Color3.Purple();

        // Our built-in 'sphere' shape.
        var sphere = MeshBuilder.CreateSphere("sphere", {diameter: 2, segments: 32}, scene);

        // Move the sphere upward 1/2 its height
        sphere.position.y = 1;

        // Our built-in 'ground' shape.
        var ground = MeshBuilder.CreateGround("ground", {width: 6, height: 6}, scene);
        ground.position.z = 0;

        const gui = GUI.AdvancedDynamicTexture.CreateFullscreenUI('myUI');
        const dpr = window.devicePixelRatio;
        const rect = canvas.getBoundingClientRect();
        gui.idealWidth = rect.width;
        gui.renderScale = dpr;

        const button1 = GUI.Button.CreateSimpleButton('button', 'Start Game');
        button1.top = '-60px';
        button1.left = '0px';
        button1.width = '150px';
        button1.height = '50px';
        button1.cornerRadius = 12 * dpr;
        button1.thickness = 4 * dpr;
        button1.children[0].color = '#FFFFFF';
        button1.children[0].fontSize = 24;
        button1.children[0].fontFamily = 'Titillium Web';
        button1.color = '#0060a0';
        button1.background = '#0099FF';

        gui.addControl(button1);

        const button = GUI.Button.CreateSimpleButton('button', 'Exit');
        button.top = '0px';
        button.left = '0px';
        button.width = '150px';
        button.height = '50px';
        button.cornerRadius = 12 * dpr;
        button.thickness = 4 * dpr;
        button.children[0].color = '#FFFFFF';
        button.children[0].fontSize = 24;
        button.children[0].fontFamily = 'Titillium Web';
        button.color = '#0060a0';
        button.background = '#0099FF';

        button.onPointerClickObservable.add(() =>
        {
            require('electron').remote.app.quit();
        });

        gui.addControl(button);

        return scene;
    } // end createScene

    render(engine : Engine, scene : Scene)
    {
        // Run the render loop.
        engine.runRenderLoop(() =>
        {
            scene.render();
        });

        // The canvas/window resize event handler.
        window.addEventListener('resize', () =>
        {
            engine.resize();
        });
    } // end render
} // end BaseGame

// ---------------------------------------------------------------------------------------------------------------------

export default new ExampleGame();

// ---------------------------------------------------------------------------------------------------------------------
