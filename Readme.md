# SAGE Example Game

Just a simple example of using SAGE to write a basic game. This repo tries to
showcase the major bits of functionality.

## Running the Example

Run the example with `npm start`.`
